﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aggro : MonoBehaviour {
    // Important note: range needs to be set to the square of the actual distance
    // This is to avoid square root calculations when doing vector math
    [SerializeField] public int range;
    private GameObject[] heroes;
    private GameObject[] enemies;
    private Aggro enemyAggro;
    public GameObject closest; // Need to make this unable to be modified eventually
    private float closestDist;

    private Vector3 startPos;
    private Vector3 aggroPos;
    private Vector3 thisPos;
    private Vector3 targetPos;
    private Vector3 currVec;
    private float currDist;

	// Use this for initialization
	void Start () {
        startPos = this.transform.position;
        aggroPos = startPos;
        heroes = GameObject.FindGameObjectsWithTag("Player");
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        closest = null;
        closestDist = 50000;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (heroes.Length > 0)
        {
            closest = findClosest();
        }
        else
        {
            closest = null;
        }
	}
// Implement aggro chaining as well
    // determines the closest target to be aggroed onto
    private GameObject findClosest()
    {
        GameObject closeTarget = closest;
        thisPos = this.transform.position;

        if (closeTarget != null)
        {
            currVec = aggroPos - closeTarget.transform.position;
            closestDist = currVec.sqrMagnitude;
        }

        foreach (GameObject hero in heroes)
        {
            targetPos = hero.transform.position;
            currVec = aggroPos - targetPos;
            currDist = currVec.sqrMagnitude;
// These references to closestDist should be in reference to the spawn point, not current location
            // Prevents aggro on heroes too far above or below and selects closest target
            if ((currVec.y < 3 && currVec.y > -3) && currDist < range && currDist < closestDist)
            {
                closestDist = currDist;
                closeTarget = hero;

                foreach (GameObject enemy in enemies)
                {
                    if ((enemy.transform.position - thisPos).sqrMagnitude < range)
                    {
                        enemyAggro = enemy.GetComponent<Aggro>();
                        enemyAggro.aggroPos = thisPos;
                        enemyAggro.closest = hero;
                    }
                }
            }
        }

        // Give buffer room before losing aggro to avoid jittering
        if (closeTarget != null && closestDist > (range * 1.1))
        {
            closestDist = 50000;
            closeTarget = null;
            aggroPos = startPos;
        }
        return closeTarget;
    }
}
