﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathing : MonoBehaviour {
    [SerializeField] public int style;
    [SerializeField] float speed;
    [SerializeField] float attackDistance;
    Aggro aggroScript;
    public Transform targetLoc;
    public Vector3 startRot;
    public Vector3 startPos;

    public bool canMove;
    public float distance;
    private UnityEngine.AI.NavMeshAgent agent;
    // Use this for initialization
    void Start ()
    {
        aggroScript = this.GetComponent<Aggro>();
        startRot = this.transform.eulerAngles;
        startPos = this.transform.position;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        distance = 9001;
        canMove = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (aggroScript.closest != null)
        {
            targetLoc = aggroScript.closest.transform;

            Vector3 direction = targetLoc.position - this.transform.position;
            distance = Mathf.Sqrt(Mathf.Pow(direction.x, 2) + Mathf.Pow(direction.z, 2));
            direction.x = direction.x / distance;
            direction.z = direction.z / distance;

            // We work in 2D planes, so only need y-axis rotation
            // This may need to change when flying enemies are implemented
            direction.y = Mathf.Atan2(direction.x, direction.z) * 180 / Mathf.PI;

            if (canMove)
            {
                switch (style)
                {
                    // simple pathfinding chase
                    case 1:
                        // Takes the most direct walking route to the target, stops at attacking range
                        if (distance >= attackDistance)
                        {
                            agent.enabled = true;
                            agent.destination = targetLoc.position;
                        }
                        else
                        {
                            agent.enabled = false;
                            //TurnAndFace(direction);
                        }
                        break;

                    // simple pathfinding projectile shoot
                    case 2:
                        // Takes the most direct walking route to the target, stops at attacking range
                        if (distance >= attackDistance)
                        {
                            agent.enabled = true;
                            agent.destination = targetLoc.position;
                        }
                        else
                        {
                            agent.enabled = false;
                        }
                        break;

                    default:
                        TurnAndFace(direction);
                        break;
                }
            }
            else
                agent.enabled = false;
        }
        else
        {
            targetLoc = null;
            // Return to original position
            agent.enabled = true;
            agent.destination = startPos;
        }
	}

    public void TurnAndFace(Vector3 direction)
    {
        this.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, direction.y, 0), 5f * Time.deltaTime);
    }

    public float getAttackDistance()
    {
        return attackDistance;
    }
}
