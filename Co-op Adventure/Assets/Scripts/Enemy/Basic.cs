﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Basic : MonoBehaviour {
    [SerializeField] private int health;
    [SerializeField] private UnityEvent _onDeath;

    public void TakeDamage (int damage) {
        health -= damage;

        if (health <= 1)
            _onDeath.Invoke();
    }
}
