﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    [SerializeField] private int damage;
    [SerializeField] private int dmgLossOnPierce;
    [SerializeField] private int pierce;
    private Rigidbody body;
    [SerializeField] private string toHurt;
    private string tag;

    private void Start()
    {
        body = this.GetComponent<Rigidbody>();
    }

    void OnTriggerStay(Collider col)
    {
        // Will need to modify if friendly NPCs are added
        if (col.gameObject.tag == toHurt)
        {
            if (body.velocity.sqrMagnitude < 0.01f)
                col.gameObject.GetComponent<TakeDamage>().gotHit(damage, this.transform.position);
            else
                col.gameObject.GetComponent<TakeDamage>().gotHit(damage, col.gameObject.transform.position - body.velocity);
            pierce--;

            if (pierce < 0)
                Destroy(this.gameObject);

            if (damage > dmgLossOnPierce)
                damage -= dmgLossOnPierce;
            else
                damage = 0;
        }

        tag = col.gameObject.tag;
        if (tag != "Enemy" && tag != "Player" && tag != "Projectile")
            Destroy(this.gameObject);
    }
}
