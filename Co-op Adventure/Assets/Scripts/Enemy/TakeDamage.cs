﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TakeDamage : MonoBehaviour {
    private Basic stats;
    [SerializeField] UnityEvent knockbackAnimation;

    private float knockbackTime;
    private bool knockbackDone;
    private Vector3 knockbackDirection;
    private float invincibilityTime;

    private Player player;
    private Pathing enemy;
    private Rigidbody body;

    // 1 is a player, 2 is an enemy
    private int type;

	// Use this for initialization
	void Start () {
        stats = this.GetComponent<Basic>();
        body = this.GetComponent<Rigidbody>();
        knockbackTime = 0;
        invincibilityTime = 0;

        if (this.gameObject.CompareTag("Player"))
        {
            player = this.GetComponent<Player>();
            type = 1;
        }
        else if (this.gameObject.CompareTag("Enemy"))
        {
            enemy = this.GetComponent<Pathing>();
            type = 2;
        }
        else
            type = 0;

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (knockbackTime > 0)
        {
            knockbackDone = true;
            if (type == 1)
            {
                player.CanMove = false;
                player.Move(knockbackDirection * -1);
            }
            else if (type == 2)
            {
                enemy.canMove = false;
                body.isKinematic = false;
                body.AddForce(knockbackDirection * -5000f);
            }

            knockbackTime -= Time.deltaTime;
        }
        else if (knockbackDone)
        {
            knockbackDone = false;
            if (type == 1)
                player.CanMove = true;
            else if (type == 2)
                enemy.canMove = true;
        }

        if (invincibilityTime > 0)
        {
            invincibilityTime -= Time.deltaTime;
        }
	}

    public void gotHit(int damage, Vector3 source)
    {
        if (invincibilityTime <= 0)
        {
            stats.TakeDamage(damage);
            knockbackAnimation.Invoke();
            knockbackTime = 0.1f;
            invincibilityTime = 1f;
            knockbackDirection = (source - this.transform.position).normalized;
            knockbackDirection.y = 0;
        }
    }
}
