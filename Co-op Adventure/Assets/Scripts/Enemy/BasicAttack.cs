﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Animations;

public class BasicAttack : MonoBehaviour {
    [SerializeField] UnityEvent _atDistance;
    [SerializeField] bool attackFinished;
    [SerializeField] private int damage;
    [SerializeField] private float duration;
    [SerializeField] private float delay;
    [SerializeField] private float delayTime;
    [SerializeField] private float durationTime;
    private int styleStore;

    private Pathing pathingScript;
    private float attackDistance;

    private GameObject target;
    private Aggro aggroScript;

	// Use this for initialization
	void Start ()
    {
        if (delay < duration)
            Debug.Log("Warning: delay between attacks needs to be longer than the duration of the attack");
        pathingScript = this.GetComponent<Pathing>();
        styleStore = pathingScript.style;
        attackDistance = pathingScript.getAttackDistance();
        aggroScript = this.GetComponent<Aggro>();
        attackFinished = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (aggroScript.closest != null)
        {
            target = aggroScript.closest;

            // Within distance and attack animation finished playing out
            if (pathingScript.distance < attackDistance && durationTime <= 0)
            {
                // Does damage if within range at the end of the animation
                if (attackFinished)
                {
                    target.GetComponent<TakeDamage>().gotHit(damage, this.transform.position);
                    attackFinished = false;
                }
                // Starts the next attack if enough time has passed since the last one
                else if (delayTime <= 0)
                {
                    attackFinished = true;
                    durationTime = duration;
                    _atDistance.Invoke();
                    delayTime = delay;
                    pathingScript.style = 0;
                }
            }
        }

        // Counts down until attack animation is over
        if (durationTime > 0)
        {
            durationTime -= Time.deltaTime;
        }
        else
        {
            attackFinished = false;
            pathingScript.style = styleStore;
        }

        // Counts down until next attack can happen
        if (delayTime > 0)
        {
            delayTime -= Time.deltaTime;
        }
    }
}
