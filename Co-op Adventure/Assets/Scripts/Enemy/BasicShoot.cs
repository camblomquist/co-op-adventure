﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Animations;

public class BasicShoot : MonoBehaviour {
    [SerializeField] private GameObject _shot;
    [SerializeField] private GameObject spawnSpot;
    [SerializeField] private float launchSpeed;
    [SerializeField] private UnityEvent _atDistance;
    private bool attackFinished;
    [SerializeField] private float duration;
    [SerializeField] private float delay;
    [SerializeField] private float shotTimeout;
    private float delayTime;
    private float durationTime;
    private int styleStore;

    private Pathing pathingScript;
    private float attackDistance;

    private Aggro aggroScript;

    // Use this for initialization
    void Start ()
    {
        pathingScript = this.GetComponent<Pathing>();
        styleStore = pathingScript.style;
        attackDistance = pathingScript.getAttackDistance();
        aggroScript = this.GetComponent<Aggro>();
        attackFinished = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (aggroScript.closest != null)
        {
            if (pathingScript.distance < attackDistance && durationTime <= 0)
            {
                if (attackFinished)
                {
                    attackFinished = false;
                    GameObject shot = Instantiate(_shot, spawnSpot.transform.position, transform.rotation);
                    shot.GetComponent<Rigidbody>().velocity = transform.forward * launchSpeed;
                    Destroy(shot, shotTimeout);
                }
                else if (delayTime <= 0)
                {
                    attackFinished = true;
                    _atDistance.Invoke();
                    durationTime = duration;
                    delayTime = delay;
                    pathingScript.style = 0;
                }
            }
        }

        if (durationTime > 0)
        {
            durationTime -= Time.deltaTime;
        }
        else
        {
            attackFinished = false;
            pathingScript.style = styleStore;
        }

        if (delayTime > 0)
        {
            delayTime -= Time.deltaTime;
        }
    }
}
