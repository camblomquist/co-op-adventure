﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CompileNotifier : EditorWindow {

    [MenuItem("Window/Compile Notifier")]
    public static void Init()
    {
        var window = GetWindow(typeof(CompileNotifier));
        window.Show();
    }

    public void OnGUI()
    {
        EditorGUILayout.LabelField(EditorApplication.isCompiling ? "Compiling" : "");
        Repaint();
    }
}
