﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

[ExecuteInEditMode]
public class CameraTarget : MonoBehaviour
{

	[SerializeField] private Transform _targetA;

	[SerializeField] private Transform _targetB;

	private void Start()
	{
		transform.position = (_targetA.position + _targetB.position) / 2;
	}
	
	private void LateUpdate ()
	{
		Vector3 target = (_targetA.position + _targetB.position) / 2;

		transform.position = target;
	}
}
