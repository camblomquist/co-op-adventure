﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{

	private enum PlayerHandle
	{
		P1,
		P2
	};

	[SerializeField] private PlayerHandle _playerHandle;
	
	[SerializeField, Tooltip("How long a button must be pressed to count as a long press")] 
	private float _longPressThreshold = 0.5f;
	
	private Player _player;

	private bool _canMove = true; // Needed to ensure that Input doesn't override other components requests

	private Vector3 _directional;

	private void Awake()
	{
		_player = GetComponent<Player>();
	}

	// Update is called once per frame
	private void Update ()
	{
		// Super nasty string building, consider doing something to prevent this
		_directional = new Vector3(Input.GetAxis(_playerHandle + "H"), 0, Input.GetAxis(_playerHandle + "V"));
		float magnitude = Mathf.Max(Mathf.Abs(_directional.x), Mathf.Abs(_directional.z));
		_directional.Normalize();

		Vector3 movement = Vector3.zero;
		
		if (_canMove && Input.GetButtonDown(_playerHandle + "A1"))
		{
			_canMove = false;
			_player.CanMove = false; // Can argue whether or not this should be handled by Player
			StartCoroutine(Press());
		}
		else if (_canMove && Input.GetButtonDown(_playerHandle + "A2"))
		{
			_player.ActivateAltAction();
		}
		
		if (_player.CanMove)
		{
			_canMove = true;

			if (_directional.sqrMagnitude > 0)
			{
				transform.rotation = Quaternion.LookRotation(_directional.normalized);
				movement = _directional * magnitude * _player.Speed;
			}
			
			_player.Animator.SetFloat("Walk", movement.magnitude);
		}
		
		// Keep this outside loop just so Player gets updated every frame
		_player.Move(movement * Time.deltaTime);
	}

	// Could probably do this more efficiently without making a coroutine
	private IEnumerator Press()
	{
		_player.Animator.SetBool("Attack", true);
		float holdTime = 0f;

		while (Input.GetButton(_playerHandle + "A1"))
		{
			float dt = Time.deltaTime;
			holdTime += dt;

			if (holdTime > _longPressThreshold)
			{
				_player.SetAimTargetActive(true);
				
				// Nested for additional logic later
				if (_directional.sqrMagnitude > 0)
				{
					_directional = new Vector3(Input.GetAxisRaw(_playerHandle + "H"), 0, Input.GetAxisRaw(_playerHandle + "V"));
					_player.MoveAimTarget(_directional.normalized * _player.Speed);
				}
			}
			yield return null;
		}

		_player.Animator.SetBool("Attack", false);
		yield return new WaitForSeconds(Mathf.Clamp(1 - holdTime, 0.1f, 0.5f));
		if (holdTime < _longPressThreshold)
		{
			_player.ActivateMainAction();
		}
		else
		{
			_player.ActivateMainChargedAction(holdTime - _longPressThreshold);
		}
		
		_player.SetAimTargetActive(false);
		_player.ResetAimTarget();
		
		// Reenable movement
		yield return new WaitForSeconds(0.25f);
		_player.CanMove = true;
	}
}
