﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using StandardAssets.Characters.Physics;
using UnityEditor.ShaderGraph;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

[RequireComponent(typeof(OpenCharacterController))]
public class Player : MonoBehaviour
{

	public float Speed;
	
	public bool UseGravity = true;

	[SerializeField] private Transform _aimTargetOrigin;
	
	[SerializeField] private Transform _aimTarget;

	[SerializeField] private float _maxAimDistance;

	[SerializeField] private float _terminalVelocity;

	[SerializeField] private Camera _camera;

	[SerializeField] private PlayerAction _mainAction;
	
	[SerializeField] private List<PlayerAction> _specials;
	
	[SerializeField] private PlayerAction _defaultAltAction;
	

	private PlayerAction _currentSpecial;

	private PlayerAction _altAction;

	private OpenCharacterController _occ;

	private OpenCharacterController _aimocc;

	private Rigidbody _rb;

	private Animator _anim;

	private Vector3 _movement;

	private Vector3 _gravity;

	private Vector3 _cameraCorrectionVelocity;

	private Vector3 _platformMovement;

	private Transform _currentMovingPlatform;

	private Transform _lastMovingPlatform;

	private Vector3 _lastPlatformPosition;

	
	public bool CanMove { get; set; }
	
	public Vector3 AimTargetPosition => _aimTarget.position;

	public Animator Animator => _anim;

	public void Move(Vector3 movement)
	{
		_movement = movement;
		//UpdateMovingPlatformMovement();
		UpdateGravity();
		_occ.Move(_movement);
		PreventOffCameraMovement();
	}

	public void ActivateMainAction()
	{
		_mainAction.ActivateAction(this);
	}

	public void ActivateMainChargedAction(float time)
	{
		_mainAction.ActivateChargedAction(this, time);
	}
	
	// The special to perform on a Short Press
	public void ActivateSpecial()
	{
		_currentSpecial.ActivateAction(this);
	}

	// Switches the action
	public void SwitchSpecial(int index)
	{
		// Allow for looping without forcing the caller to figure it out
		if (index < 0)
		{
			index = _specials.Count - 1;
		}

		if (index >= _specials.Count)
		{
			index = 0;
		}

		_currentSpecial = _specials[index];
	}

	public void AddSpecial(PlayerAction action, bool switchTo = false)
	{
		_specials.Add(action);
		
		if (switchTo)
		{
			_currentSpecial = _specials.Last(); // Ew LINQ
		}
	}

	public void RemoveSpecial(PlayerAction action)
	{
		if (_specials.Remove(action))
		{
			if (_currentSpecial == action)
			{
				_currentSpecial = _specials[0];
			}
		}
	}

	// Typically for context-sensitive actions
	public void ActivateAltAction()
	{
		if (_altAction != null)
		{
			_altAction.ActivateAction(this);
		}
	}

	public void SetAltAction(PlayerAction action)
	{
		_altAction = action;
	}

	public void UnsetAltAction()
	{
		_altAction = _defaultAltAction;
	}

	// Once again, using movement per second
	public void MoveAimTarget(Vector3 movement)
	{
		Vector3 position = _aimTarget.position + movement * Time.deltaTime;

		// Move the target as if it were extending from the player
		Vector3 direction = position - transform.position;
		direction.y = 0;
		
		transform.rotation = Quaternion.LookRotation(direction.normalized);
		// This is starting from a different position
		direction = position - _aimTargetOrigin.position;
		direction.y = 0;
		direction = Vector3.ClampMagnitude(direction, _maxAimDistance);
		_aimTarget.position = _aimTargetOrigin.position;
		
		// This is stupid but it works
		direction /= 10;
		for (int i = 0; i < 10; i++)
		{
			position = _aimTarget.position;
			
			_aimocc.Move(direction);
			
			// Keep the target from sliding off cliffs
			if (!Physics.Raycast(_aimTarget.position + direction.normalized * _aimocc.scaledRadius, Vector3.down,
				_aimocc.GetHeight() / 2 + _aimocc.scaledRadius + _aimocc.GetSkinWidth(), _aimocc.GetCollisionLayerMask()))
			{
				float y = position.y;
				position = _aimTarget.position;
				position.y = y;
				_aimTarget.position = position;
			}
			else if (!Physics.Raycast(_aimTarget.position - direction.normalized * _aimocc.scaledRadius, Vector3.down,
				_aimocc.GetHeight() / 2 + _aimocc.scaledRadius + _aimocc.GetSkinWidth(), _aimocc.GetCollisionLayerMask()))
			{
				float y = position.y;
				position = _aimTarget.position;
				position.y = y;
				_aimTarget.position = position;
			}
		}
	}

	public void SetAimTargetActive(bool value)
	{
		// It might be marginally faster to ensure non-redundant activation or deactivation, but not sure
		if (_aimTarget.gameObject.activeSelf != value)
		{
			_aimTarget.gameObject.SetActive(value);
		}
	}
	
	public void ResetAimTarget()
	{
		_aimTarget.position = _aimTargetOrigin.position;
	}
	
	private void Awake()
	{
		_occ = GetComponent<OpenCharacterController>();
		_anim = GetComponent<Animator>();
		_rb = GetComponent<Rigidbody>();
		_rb = GetComponent<Rigidbody>();
		CanMove = true;

		_occ.collision += OnCollision;
	}

	private void Start()
	{
		if (_camera == null)
		{
			_camera = Camera.main;
		}

		_aimocc = _aimTarget.GetComponent<OpenCharacterController>();
		SetAimTargetActive(false);
	}
	
	private void OnCollision(OpenCharacterController.CollisionInfo info)
	{
		if (info.rigidbody != null)
		{
			// Moving Platforms
			if (Vector3.Angle(Vector3.up, info.normal) < 30f)
			{
				if (_currentMovingPlatform == null )
				{
					_currentMovingPlatform = info.transform;
				}
			}
		}
	}

	// Mostly lifted from The Standard Assets code. Doesn't handle rotations
	private void UpdateMovingPlatformMovement()
	{
		if (_currentMovingPlatform == null)
		{
			if (_lastMovingPlatform != null)
			{
				RaycastHit hit;
				if (Physics.SphereCast(_occ.GetFootWorldPosition() + Vector3.up * _occ.scaledRadius, _occ.scaledRadius,
					Vector3.down, out hit, 1))
				{
					if (hit.transform == _lastMovingPlatform)
					{
						_currentMovingPlatform = _lastMovingPlatform;
					}
					else
					{
						return;
					}
				}
				else
				{
					_lastMovingPlatform = null;
					return;
				}
			}
		}

		if (_lastMovingPlatform == null)
		{
			if (_currentMovingPlatform != null)
			{
				_lastMovingPlatform = _currentMovingPlatform;
				_lastPlatformPosition = _currentMovingPlatform.position;
			}

			return;
		}

		_platformMovement = _currentMovingPlatform.position - _lastPlatformPosition;

		_lastPlatformPosition = _currentMovingPlatform.position;
		
		_currentMovingPlatform = null;

		if (_platformMovement != Vector3.zero)
		{
			_occ.Move(_platformMovement);
			_platformMovement = Vector3.zero;
		}
	}
	
	private void UpdateGravity()
	{
		if (!UseGravity)
		{
			_gravity = Vector3.zero;
			return;
		}

		RaycastHit hit;
		float radius = _occ.scaledRadius + _occ.GetSkinWidth();
		// OCC sees ground and there's ground immediately beneath us (with a bit of leeway for slopes and stairs)
		if (_occ.isGrounded && Physics.Raycast(new Ray(_occ.GetFootWorldPosition(), Vector3.down), radius,
			    _occ.GetCollisionLayerMask()))
		{
			_gravity = Vector3.zero;
		}
		// Cancel movement if there truly isn't any grounding (still sticks in some edge cases)
		else if (_occ.isGrounded && Physics.SphereCast(_occ.GetFootWorldPosition() + Vector3.up * radius, 
			         radius, Vector3.down, out hit, radius,
			         _occ.GetCollisionLayerMask()))
		{
			Vector3 heading = hit.normal * Time.deltaTime;
			heading.y = 0;
			Debug.DrawRay(hit.point, heading / Time.deltaTime, Color.red);
			_gravity.y = Physics.gravity.y * Time.deltaTime;
			_gravity.Set(heading.x, _gravity.y, heading.z);
		}
		else
		{
			_gravity.y = Mathf.Clamp(_gravity.y + Physics.gravity.y * Time.deltaTime, -_terminalVelocity, 0);
			_gravity.Set(0, _gravity.y, 0);
			_movement = Vector3.zero;
		}

		if (_gravity != Vector3.zero)
		{
			_occ.Move(_gravity * Time.deltaTime);
		}
	}

	private void PreventOffCameraMovement()
	{
		RaycastHit hit;
		float top = transform.position.z;
		float bottom = transform.position.z;
		float left = transform.position.x;
		float right = transform.position.x;

		LayerMask mask = ~LayerMask.GetMask("Player");
		// Left and Bottom
		if(Physics.Raycast(_camera.ViewportPointToRay(new Vector3(0.1f, 0.1f)), out hit, _camera.farClipPlane, mask))
		{
			Debug.DrawRay(hit.point, Vector3.up, Color.magenta);
			left = hit.point.x;
			bottom = hit.point.z;
		}
		// Right
		if(Physics.Raycast(_camera.ViewportPointToRay(new Vector3(0.9f, 0.1f)), out hit, _camera.farClipPlane, mask))
		{
			Debug.DrawRay(hit.point, Vector3.up, Color.magenta);
			right = hit.point.x;
		}
		// Top
		if(Physics.Raycast(_camera.ViewportPointToRay(new Vector3(0.5f, 0.8f)), out hit, _camera.farClipPlane, mask))
		{
			Debug.DrawRay(hit.point, Vector3.up, Color.magenta);
			top = hit.point.z;
		}
		
		
		Vector3 cameraCorrection = Vector3.zero;

		cameraCorrection.x = Mathf.Clamp(transform.position.x, left, right) - transform.position.x;
		cameraCorrection.z = Mathf.Clamp(transform.position.z, bottom, top) - transform.position.z;
		
		if (cameraCorrection != Vector3.zero)
		{
			_occ.Move(cameraCorrection);
		}
	}
}
