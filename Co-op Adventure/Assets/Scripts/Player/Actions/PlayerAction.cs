﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerAction : MonoBehaviour
{
    
    public abstract void ActivateAction(Player player);

    public abstract void ActivateChargedAction(Player player, float time);
}
