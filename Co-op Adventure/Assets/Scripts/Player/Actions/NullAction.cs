﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NullAction : PlayerAction {
    
    public override void ActivateAction(Player player)
    {
        
    }

    public override void ActivateChargedAction(Player player, float time)
    {
        
    }
}
