﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadAction : PlayerAction
{

	[SerializeField, Tooltip("The object being read")] private Transform _target;
	[SerializeField, Tooltip("The collider to test against")] private Collider _collider;
	[SerializeField] private GameObject _thing;
	
	public override void ActivateAction(Player player)
	{
		player.CanMove = false;
		// If there's a better way to check that we're facing the sign, let me know
		Vector3 position = player.transform.position;
		position.y = _target.position.y;
		float dist = Vector3.Distance(player.transform.position, _target.position);
		Debug.DrawRay(player.transform.position, player.transform.forward * dist, Color.black, 5);
		RaycastHit hit;
		if (_collider.Raycast(new Ray(position, player.transform.forward), out hit, dist))
		{
			StartCoroutine(Display());
		}

		player.CanMove = true;
	}

	public override void ActivateChargedAction(Player player, float time) { }

	private IEnumerator Display()
	{
		_thing.SetActive(true);
		yield return new WaitForSeconds(0.5f);
		_thing.SetActive(false);
	}
}
