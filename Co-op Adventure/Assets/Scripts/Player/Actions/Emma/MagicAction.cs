﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicAction : PlayerAction
{

    [SerializeField] private GameObject _effect;
    [SerializeField] private float _fireTime;
    [SerializeField] private float _maxChargeTime;
    [SerializeField] private float _chargeMultiplier;
    
    public override void ActivateAction(Player player)
    {
        GameObject fireball = Instantiate(_effect, player.AimTargetPosition, Quaternion.identity);
        Destroy(fireball, _fireTime);
    }

    public override void ActivateChargedAction(Player player, float time)
    {
        GameObject fireball = Instantiate(_effect, player.AimTargetPosition, Quaternion.identity);
        float scale = Mathf.Clamp(_chargeMultiplier * time / _maxChargeTime, 1, _chargeMultiplier);
        fireball.transform.localScale *= scale;
        Destroy(fireball, _fireTime);
    }
}
