﻿using Poly2Tri;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class LinearMovingPlatform : MonoBehaviour
{

	[SerializeField] private float _speed;

	[SerializeField, Tooltip("How long the platform will stay at a waypoint.")]
	private float _waitTime;
	
	[SerializeField] private Transform[] _waypoints;
	
	private Rigidbody _rb;

	private Vector3 _activePoint;
	private int _index;
	private float _time;
	
	private void Awake()
	{
		_rb = GetComponent<Rigidbody>();
		_rb.isKinematic = true;

		_activePoint = _waypoints[0].position;
	}

	private void FixedUpdate()
	{
		if (_time > _waitTime)
		{
			if (_rb.position != _activePoint)
			{
				// MovePosition will modify the velocity, but does some interpolation that produces jitters in objects riding the platform
				_rb.MovePosition(Vector3.MoveTowards(_rb.position, _activePoint, _speed * Time.deltaTime));
				//_rb.position = Vector3.MoveTowards(_rb.position, _activePoint, _speed * Time.deltaTime);
				//_rb.velocity = _speed * (_activePoint - _rb.position).normalized;
			}
			else
			{
				_index = (_index + 1) % _waypoints.Length;
				_activePoint = _waypoints[_index].position;
				//_rb.velocity = Vector3.zero;
				_time = 0;
			}
		}

		_time += Time.deltaTime;
	}
}
