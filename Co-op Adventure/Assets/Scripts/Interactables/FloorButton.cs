﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/* With extra effort, this could be replaced with a combination of more generic scripts, but then there exists many
 * generic scripts that perform a single task and likely will only be used a handful of times */

[RequireComponent(typeof(Rigidbody))]
public class FloorButton : MonoBehaviour
{

	[SerializeField] private bool _stayPressed;
	[SerializeField, Tooltip("How far the button must travel to be considered pressed")] 
	private float _actuation;
	[SerializeField, Tooltip("The maximum distance the button may travel")] 
	private float _travelDistance;
	[SerializeField, Tooltip("How long it takes for the button to bottom out")] 
	private float _travelTime;
	
	[SerializeField] private UnityEvent _onPress;
	[SerializeField] private UnityEvent _onUnpress;
	
	private Rigidbody _rb;
	private Coroutine _activeRoutine;
	private WaitForFixedUpdate _wait;
	private Vector3 _start;
	
	private void Awake()
	{
		_rb = GetComponent<Rigidbody>();
		// Just in case
		_rb.isKinematic = true;
		
		_wait = new WaitForFixedUpdate();

		_start = _rb.position;
	}

	private void OnTriggerEnter(Collider other)
	{

		if (_activeRoutine != null)
		{
			StopCoroutine(_activeRoutine);
		}

		_activeRoutine = StartCoroutine(Press());
	}

	private void OnTriggerExit(Collider other)
	{
		if (_activeRoutine != null)
		{
			StopCoroutine(_activeRoutine);
		}
		_activeRoutine = StartCoroutine(Unpress());
	}

	private IEnumerator Press()
	{
		float speed = _travelDistance / _travelTime;
		Vector3 end = _start + Vector3.down * _actuation;
		
		while (_rb.position != end)
		{
			_rb.MovePosition(Vector3.MoveTowards(_rb.position, end, speed * Time.fixedDeltaTime));
			yield return _wait;
		}
		
		_onPress.Invoke();
		end = _start + Vector3.down * _travelDistance;
		
		while (_rb.position != end)
		{
			_rb.MovePosition(Vector3.MoveTowards(_rb.position, end, speed * Time.fixedDeltaTime));
			yield return _wait;
		}
	}

	private IEnumerator Unpress()
	{
		float speed = _travelDistance / _travelTime;
		Vector3 end = _start + Vector3.down * _actuation;
		
		while (_rb.position != end)
		{
			_rb.MovePosition(Vector3.MoveTowards(_rb.position, end, speed * Time.fixedDeltaTime));
			yield return _wait;
		}

		if (!_stayPressed)
		{
			_onUnpress.Invoke();
		}

		end = _start;
		
		while (_rb.position != end)
		{
			_rb.MovePosition(Vector3.MoveTowards(_rb.position, end, speed * Time.fixedDeltaTime));
			yield return _wait;
		}
	}
}
