﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class KeyedDoor : MonoBehaviour
{

	[SerializeField] private int _keyCount;
	[SerializeField, Tooltip("If true, door will not re-close if a key is added")] private bool _stayOpen;

	[SerializeField] private UnityEvent _onOpen;
	[SerializeField] private UnityEvent _onClose;

	public void AddKey()
	{
		int previous = _keyCount;
		_keyCount++;

		if (!_stayOpen && previous == 0 && _keyCount > 0)
		{
			Close();
		}
	}

	public void RemoveKey()
	{
		int previous = _keyCount;
		_keyCount = Mathf.Clamp(_keyCount--, 0, _keyCount);
		
		if (_keyCount == 0 && previous != 0)
		{
			Open();
		}
	}

	private void Open()
	{
		_onOpen.Invoke();
	}

	private void Close()
	{
		_onClose.Invoke();
	}
}
