﻿using UnityEngine;

public class ContextActionTrigger : MonoBehaviour
{

	[SerializeField] private PlayerAction _action;

	private void OnTriggerEnter(Collider col) 
	{
		col.GetComponent<Player>().SetAltAction(_action);
	}

	private void OnTriggerExit(Collider col)
	{
		col.GetComponent<Player>().UnsetAltAction();
	}
}
