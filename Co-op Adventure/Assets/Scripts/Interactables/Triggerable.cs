﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Triggerable : MonoBehaviour {

	[SerializeField, Tooltip("Optional list of valid tags for collision")] private string[] _tags;
	[SerializeField] private UnityEvent _onEnter;
	[SerializeField] private UnityEvent _onExit;

	private void OnTriggerEnter(Collider other)
	{
		if (_tags.Length == 0 || _tags.Any(tag => other.gameObject.CompareTag(tag)))
		{
			_onEnter.Invoke();
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (_tags.Length == 0 || _tags.Any(tag => other.gameObject.CompareTag(tag)))
		{
			_onExit.Invoke();
		}
	}
}
