﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New StringVar", menuName = "Variable/StringVar")]
public class StringVar : ScriptableObject
{

    [SerializeField] private string _string;
    
    private string _runtimeString;

    public string Value
    {
        get { return _runtimeString; }
        set { _runtimeString = value; }
    }
    
    private void Awake()
    {
        _runtimeString = _string;
    }
}
