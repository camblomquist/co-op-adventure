﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SpriteVar", menuName = "Variable/SpriteVar")]
public class SpriteVar : ScriptableObject
{

    [SerializeField] private Sprite _sprite;

    private Sprite _runtimeSprite;

    public Sprite Value
    {
        get { return _runtimeSprite; }
        set { _runtimeSprite = value; }
    }

    private void Awake()
    {
        _runtimeSprite = _sprite;
    }
}
