﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New IntVar", menuName = "Variable/IntVar")]
public class IntVar : ScriptableObject {
    
    [SerializeField] private int _int;

    private int _runtimeInt;

    public int Value
    {
        get { return _runtimeInt; }
        set { _runtimeInt = value; }
    }

    private void Awake()
    {
        _runtimeInt = _int;
    }
}
