﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DemoReload : MonoBehaviour
{

	private bool reloaded;
	void Start()
	{
		reloaded = false;
	}
	
	void Update () {
		// I don't think this will continue to run after starting the scene load, but just in case.
		if (Input.GetKeyDown(KeyCode.Escape) && !reloaded)
		{
			reloaded = true;
			SceneManager.LoadScene(0);
		}
	}
}
